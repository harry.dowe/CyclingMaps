<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('authorisation', 'StravaController@authorisation');
Route::get('token_exchange', 'StravaController@tokenExchange')->name('token_exchange');

Route::group(['middleware' => 'auth'], function () {
    Route::get('images/maps/{stravaId}', 'ImagesController@show');
});

Route::get('activities/{stravaId}/elevation', 'StravaController@showElevation');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

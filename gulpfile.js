const elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
    mix.sass('app.scss')
        .copy('resources/assets/js/app.js', 'public/js/app.js')
        .copy('resources/assets/js/elevation.js', 'public/js/elevation.js')
        .copy('resources/assets/img', 'public/img/');
});

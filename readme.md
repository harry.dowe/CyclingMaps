# CyclingMaps

This is an application utilising activity data imported from [Strava](https://strava.com) into [Algolia](https://algolia.com) to perform rapid searches on your data.

Here are some of the awesome API's & tools used:

* [Laravel](https://laravel.com/)
* [Strava](https://strava.github.io/api/)
* [Algolia](https://www.algolia.com/doc/rest-api/search)

## License

This is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).

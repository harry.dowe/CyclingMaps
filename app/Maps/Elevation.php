<?php

namespace App\Maps;

use App\Helpers\Maps;
use Illuminate\Support\Facades\Config;
use GuzzleHttp\Client as GuzzleClient;

class Elevation
{

    /**
     * @param string $polyline
     *
     * @return array
     */
    public static function get(string $polyline)
    {
        $client = new GuzzleClient();
        $request = $client->request('GET', self::getUrl($polyline));

        return json_decode($request->getBody(), true)['results'];
    }

    /**
     * @param array $elevations
     *
     * @return array
     */
    public static function chart(array $elevations)
    {
        $total = 0;
        $elevationArray = [['Distance', 'Elevation']];
        foreach ($elevations as $key => $elevation) {
            if (!isset($elevations[$key + 1])) {
                break;
            }

            $distance = app()->make(\LineString::class, [
                [
                    app()->make(\Point::class, [$elevation['location']['lng'], $elevation['location']['lat']]),
                    app()->make(\Point::class, [$elevations[$key + 1]['location']['lng'], $elevations[$key + 1]['location']['lat']])
                ]
            ]);

            $distance = $distance->greatCircleLength();

            $elevationArray[] = [$total, $elevation['elevation']];
            $total += $distance;
        }

        $elevationArray[] = [$total, $elevations[count($elevations) - 1]['elevation']];

        return $elevationArray;
    }

    /**
     * @param string $polyline
     *
     * @return string
     */
    private static function getUrl(string $polyline): string
    {
        $googleConfig = Config::get('google.elevation');

        $params = [
            'key' => Config::get('google.apiKey'),
            'locations' => Maps::parseParams(['enc' => $polyline])
        ];

        $encodedParams = http_build_query($params);

        return sprintf('%s?%s', $googleConfig['url'], $encodedParams);
    }
}

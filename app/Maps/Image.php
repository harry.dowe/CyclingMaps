<?php

namespace App\Maps;

use App\Helpers\Maps;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image as InterventionImage;
use Ramsey\Uuid\Uuid;

class Image
{

    /**
     * @param string $uuid
     * @param string $polyline
     *
     * @return \Intervention\Image\Image
     */
    public static function get(string $uuid, string $polyline): \Intervention\Image\Image
    {
        $path = self::getPath($uuid);

        if (File::exists($path)) {
            return InterventionImage::make(File::get($path));
        }

        return InterventionImage::make(self::getUrl($polyline))
            ->save($path);
    }

    /**
     * Generates an unused Uuid.
     *
     * @return string
     */
    public static function getUuid(): string
    {
        $uuid = Uuid::uuid4()->toString();

        while (File::exists(self::getPath($uuid))) {
            $uuid = Uuid::uuid4()->toString();
        }

        return $uuid;
    }

    /**
     * @param string $uuid
     *
     * @return string
     */
    private static function getPath(string $uuid): string
    {
        return sprintf('%s/%s.png', Config::get('maps.storage'), $uuid);
    }

    /**
     * @param string $polyline
     *
     * @return string
     */
    private static function getUrl(string $polyline): string
    {
        $googleConfig = Config::get('google.staticMaps');

        $params = [
            'key' => Config::get('google.apiKey'),
            'scale' => $googleConfig['scale'],
            'size' => sprintf('%dx%d', $googleConfig['width'], $googleConfig['height']),
            'path' => Maps::parseParams($googleConfig['path'] + ['enc' => $polyline])
        ];

        $style = Maps::getStyle($googleConfig['style']);

        $encodedParams = http_build_query($params) . $style;

        $signature = self::getSignature($encodedParams);

        return sprintf('%s%s?%s&signature=%s', $googleConfig['url'], $googleConfig['url_path'], $encodedParams, $signature);
    }

    /**
     * @param string $encodedParams
     *
     * @return string
     */
    private static function getSignature(string $encodedParams): string
    {
        return Maps::signUrl(Config::get('google.staticMaps.url_path') . '?' . $encodedParams, Config::get('google.staticMaps.urlSecret'));
    }

}

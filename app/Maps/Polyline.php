<?php

namespace App\Maps;

use emcconville\Polyline\GoogleTrait;

class Polyline
{
    use GoogleTrait;
}

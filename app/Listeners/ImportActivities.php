<?php

namespace App\Listeners;

use App\Jobs\FetchPolyline;
use App\Maps\Image as MapImage;
use App\Model\Activity;
use App\Model\User;
use Iamstuartwilson\StravaApi;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Config;

class ImportActivities implements ShouldQueue
{
    /**
     * @var StravaApi
     */
    private $strava;

    /**
     * @var User
     */
    private $user;

    /**
     * Create the event listener.
     *
     * @param StravaApi $strava
     *
     * @internal param ImportActivitiesHandler $handler
     */
    public function __construct(StravaApi $strava)
    {
        $this->strava = $strava;
    }

    /**
     * Handle the event.
     *
     * @param  Registered $event
     *
     * @return void
     * @throws \Exception
     */
    public function handle(Registered $event)
    {
        $this->user = $event->user;

        $this->strava->setAccessToken($this->user->accessToken->access_token);

        $perPage = Config::get('strava.perPage');

        $page = 1;
        while(true) {
            $activities = $this->strava->get('activities',
                ['per_page' => $perPage, 'page' => $page]);

            if ($this->strava->lastRequestInfo['http_code'] !== 200) {
                throw new \Exception(sprintf('Strava returned with HTTP code %s %s',
                    $this->strava->lastRequestInfo['http_code'], $activities->message));
            }

            $this->storeActivities($activities);

            if (count($activities) < $perPage) {
                break;
            }

            $page++;
        }
    }

    /**
     * @param array $activities
     */
    private function storeActivities($activities)
    {
        foreach ($activities as $activity) {
            $storedModel = Activity::where('strava_id', $activity->id)->first();

            if (isset($storedModel)) {
                continue;
            }

            $model = new Activity(json_decode(json_encode($activity), true));

            $model->user_id = $this->user->id;
            $model->strava_id = $activity->id;

            $model->start_date_local = new \DateTime($activity->start_date_local);
            $model->start_lat = $activity->start_latlng[0];
            $model->start_lng = $activity->start_latlng[1];
            $model->end_lat = $activity->end_latlng[0];
            $model->end_lng = $activity->end_latlng[1];
            $model->polyline_summary = $activity->map->summary_polyline;
            $model->map_image = MapImage::getUuid();

            $model->save();

            if (isset($model->polyline_summary)) {
                dispatch(new FetchPolyline($model));
            }
        }
    }

}

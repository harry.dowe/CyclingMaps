<?php

namespace App\Listeners;

use App\Events\Registering;
use App\Model\User;
use Illuminate\Auth\Events\Registered;

class RegisterUser
{

    /**
     * Handle the event.
     *
     * @param  Registering  $event
     * @return void
     */
    public function handle(Registering $event)
    {
        $athlete = $event->athlete;

        $user = new User();
        $user->athlete_id = $athlete->id;
        $user->name = $athlete->username;
        $user->email = $athlete->email;
        $user->profile_medium = $athlete->profile_medium;
        $user->profile = $athlete->profile;

        $user->save();

        $user->storeAccessToken($event->accessToken);

        event(new Registered($user));
    }
}

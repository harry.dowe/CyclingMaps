<?php

namespace App\Listeners\Log;

use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class RegisteredUser implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        Log::info('User successfully registered.', [
            'user_id' => $event->user->id,
            'athlete_id' => $event->user->athlete_id
        ]);
    }
}

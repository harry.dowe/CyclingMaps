<?php

namespace App\Listeners\Log;

use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SuccessfulLogin implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        Log::info('User logged in.', [
            'user_id' => $event->user->id,
            'athlete_id' => $event->user->athlete_id
        ]);
    }
}

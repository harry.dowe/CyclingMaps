<?php

namespace App\Listeners\Log;

use App\Events\Registering;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class RegisteringUser implements ShouldQueue
{

    /**
     * Handle the event.
     *
     * @param  Registering  $event
     * @return void
     */
    public function handle(Registering $event)
    {
        Log::info('User registering.', [
            'athlete_id' => $event->athlete->id
        ]);
    }
}

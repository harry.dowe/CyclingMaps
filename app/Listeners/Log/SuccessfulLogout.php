<?php

namespace App\Listeners\Log;

use Illuminate\Auth\Events\Logout;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SuccessfulLogout implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        Log::info('User logged out.', [
            'user_id' => $event->user->id,
            'athlete_id' => $event->user->athlete_id
        ]);
    }
}

<?php

namespace App\Model;

use App\Model\Activity;
use App\Model\AccessToken;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $with = [
        'accessToken',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function accessTokens()
    {
        return $this->hasMany(AccessToken::class);
    }

    /**
     * @return mixed
     */
    public function accessToken()
    {
        return $this->hasOne(AccessToken::class)->orderBy('created_at', 'desc');
    }

    /**
     * Alias to accessToken() because laravel doesn't seem to create this
     * magic property. Have to do it manually for some reason.
     *
     * @return mixed
     */
    public function access_token()
    {
        return self::accessToken();
    }

    /**
     * @param $accessToken
     *
     * @return $this User
     */
    public function storeAccessToken(string $accessToken)
    {
        $model = new AccessToken();
        $model->access_token = $accessToken;
        $model->user()->associate($this);
        $model->save();

        return $this;
    }

}

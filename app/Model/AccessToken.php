<?php

namespace App\Model;

use App\Model\User;
use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}

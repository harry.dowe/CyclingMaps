<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Activity extends Model
{

    use Searchable;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'distance',
        'moving_time',
        'elapsed_time',
        'total_elevation_gain',
        'commute'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'start_date_local',
        'created_at',
        'updated_at',
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return array
     */
    public function toSearchableArray()
    {
        $array = [
            'strava_id' => $this->strava_id,
            'athlete_id' => $this->user->athlete_id,
            'name' => $this->name,
            'start_date_local' => (int)$this->start_date_local->format('U'),
            'distance' => $this->distance,
            'moving_time' => $this->moving_time,
            'elapsed_time' => $this->elapsed_time,
            'total_elevation_gain' => $this->total_elevation_gain,
            'commute' => $this->commute,
            'polyline_summary' => $this->polyline_summary,
            'map_image' => $this->map_image
        ];

        if (isset($this->start_lat) && isset($this->start_lng)) {
            $array['_geoloc'][] = [
                'lat' => $this->start_lat, 'lng' => $this->start_lng
            ];
        }

        if (isset($this->end_lat) && isset($this->end_lng)) {
            $array['_geoloc'][] = [
                 'lat' => $this->end_lat, 'lng' => $this->end_lng
            ];
        }

        return $array;
    }

}

<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Polyline extends Facade
{
    /**
     * @return mixed
     */
    protected static function getFacadeAccessor()
    {
        return \App\Maps\Polyline::class;
    }
}

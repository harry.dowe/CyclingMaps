<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class MapsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'maps:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Removes all maps from the file system.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $success = File::deleteDirectory(Config::get('maps.storage'), true);

        if ($success) {
            $this->line('Successfully deleted maps.');
        } else {
            $this->line('Could not delete maps directory.');
        }
    }
}

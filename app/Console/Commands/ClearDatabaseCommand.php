<?php

namespace App\Console\Commands;

use App\Model\AccessToken;
use App\Model\Activity;
use App\Model\User;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;

class ClearDatabaseCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears the database of records.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (! $this->confirmToProceed()) {
            return;
        }

        AccessToken::getQuery()->delete();
        Activity::getQuery()->delete();
        User::getQuery()->delete();

        //Activity::clearIndices(); // Removed due to incompatibility with laravel algolia & scout

        $this->info('Database cleared successfully!');
    }
}

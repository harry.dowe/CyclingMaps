<?php

namespace App\Events;

class Registering
{

    /**
     * @var string
     */
    public $accessToken;

    /**
     * @var \stdClass
     */
    public $athlete;

    /**
     * Create a new event instance.
     *
     * @param string $accessToken
     * @param \stdClass $athlete
     */
    public function __construct($accessToken, $athlete)
    {
        $this->accessToken = $accessToken;
        $this->athlete = $athlete;
    }

}

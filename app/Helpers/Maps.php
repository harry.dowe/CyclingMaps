<?php

namespace App\Helpers;

class Maps
{

    /**
     * @param array $params
     *
     * @return string
     */
    public static function parseParams(array $params): string
    {
        $path = array_map(function ($key, $value) {
            # Converts ['hello' => 'world'] to ['hello:world']
            return implode(':', [$value, $key]);
        }, $params, array_keys($params));

        return implode('|', $path);
    }

    /**
     * @param array $params
     *
     * @return string
     */
    public static function getStyle(array $params): string
    {
        $string = '';

        foreach ($params as $param) {
            $string .= '&style=';
            $string .= rawurlencode(self::parseParams($param));
        }

        return $string;
    }

    /**
     * @param string $url
     *
     * @param string $secret
     *
     * @return string
     */
    public static function signUrl(string $url, string $secret): string
    {
        $decodedSecret = StringHelper::decodeBase64UrlSafe($secret);
        $signature = hash_hmac('sha1', $url, $decodedSecret, true);

        return StringHelper::encodeBase64UrlSafe($signature);
    }

}

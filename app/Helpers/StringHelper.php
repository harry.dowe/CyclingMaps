<?php

namespace App\Helpers;

class StringHelper
{
    /**
     * @param string $value
     *
     * @return string
     */
    public static function encodeBase64UrlSafe(string $value): string
    {
        return str_replace(['+', '/'], ['-', '_'], base64_encode($value));
    }

    /**
     * @param string $value
     *
     * @return string
     */
    public static function decodeBase64UrlSafe(string $value): string
    {
        return base64_decode(str_replace(['-', '_'], ['+', '/'], $value));
    }

}

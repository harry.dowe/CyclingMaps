<?php

namespace App\Jobs;

use App\Model\Activity;
use Iamstuartwilson\StravaApi;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class FetchPolyline implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Activity
     */
    public $activity;

    /**
     * Create a new job instance.
     *
     * @param Activity $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * Execute the job.
     *
     * @param StravaApi $strava
     */
    public function handle(StravaApi $strava)
    {
        $activity = $strava->get(sprintf('activities/%s', $this->activity->strava_id));
        // TODO: Error handling
        $this->activity->polyline = $activity->map->polyline ?? $activity->map->polyline;

        $this->activity->save();
    }
}

<?php

namespace App\Providers;

use App\Http\Controllers\StravaController;
use Iamstuartwilson\StravaApi;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class StravaApiProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(StravaApi::class, function () {
            return new StravaApi(Config::get('strava.clientId'), Config::get('strava.clientSecret'));
        });
    }
}

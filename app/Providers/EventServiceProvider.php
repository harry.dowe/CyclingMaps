<?php

namespace App\Providers;

use App\Events\Registering;
use App\Listeners\AuthenticateRegisteredUser;
use App\Listeners\ImportActivities;
use App\Listeners\Log\RegisteredUser;
use App\Listeners\Log\RegisteringUser;
use App\Listeners\Log\SuccessfulLogin;
use App\Listeners\Log\SuccessfulLogout;
use App\Listeners\RegisterUser;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registering::class => [
            RegisteringUser::class,
            RegisterUser::class
        ],
        Registered::class => [
            RegisteredUser::class,
            AuthenticateRegisteredUser::class,
            ImportActivities::class
        ],
        Login::class => [
            SuccessfulLogin::class
        ],
        Logout::class => [
            SuccessfulLogout::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

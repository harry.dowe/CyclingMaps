<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class GeometryServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\Point::class, function($app, $args) {
            require_once __DIR__ . '/../../vendor/phayes/geophp/lib/geometry/Geometry.class.php';
            require_once __DIR__ . '/../../vendor/phayes/geophp/lib/geometry/Point.class.php';

            return new \Point($args[0] ?? null, $args[1] ?? null, $args[2] ?? null);
        });

        $this->app->bind(\LineString::class, function($app, $args) {
            require_once __DIR__ . '/../../vendor/phayes/geophp/lib/geometry/Geometry.class.php';
            require_once __DIR__ . '/../../vendor/phayes/geophp/lib/geometry/Collection.class.php';
            require_once __DIR__ . '/../../vendor/phayes/geophp/lib/geometry/LineString.class.php';

            return new \LineString($args[0] ?? null);
        });
    }
}

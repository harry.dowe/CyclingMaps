<?php

namespace App\Http\Controllers;

use App\Model\Activity;
use Illuminate\Support\Facades\Auth;

class PagesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (Auth::check()) {
            $user = Auth::user();

            $activityIndex = (new Activity)->searchableAs();

            return view('pages.userIndex', compact('user', 'activityIndex'));
        }

        return view('pages.guestIndex');
    }
}

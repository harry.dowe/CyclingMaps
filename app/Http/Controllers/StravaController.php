<?php

namespace App\Http\Controllers;

use App\Events\Registering;
use App\Maps\Elevation;
use App\Model\Activity;
use App\Model\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class StravaController extends BaseStravaController
{

    /**
     * @param int $stravaId
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showElevation(int $stravaId)
    {
        $activity = Activity::where('strava_id', $stravaId)->first();

        $elevationArray = Elevation::chart(Elevation::get($activity->polyline));

        $elevationJson = json_encode($elevationArray);

        return view('activity.elevation', compact('activity', 'elevationJson'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function authorisation()
    {
        return redirect($this->getStrava()->authenticationUrl(
            route('token_exchange'), 'auto', Config::get('strava.accessLevel'))
        );
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function tokenExchange(Request $request)
    {
        $this->validate($request, [
            'code' => 'required'
        ]);

        $this->authenticateUser($request->get('code'));

        return redirect('/');
    }

    /**
     * @param $code
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    private function authenticateUser($code)
    {
        $response = $this->getStrava()->tokenExchange($code);

        // TODO: response validation

        try {
            $user = User::where('athlete_id', $response->athlete->id)->firstOrFail();

            Auth::login($user, true);
        } catch (ModelNotFoundException $e) {
            event(new Registering($response->access_token, $response->athlete));
        }

        return redirect('/');
    }

}

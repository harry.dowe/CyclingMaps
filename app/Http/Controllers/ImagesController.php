<?php

namespace App\Http\Controllers;

use App\Maps\Image as MapImage;
use App\Model\Activity;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ImagesController extends Controller
{

    /**
     * @param $uuid
     *
     * @return Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function show($uuid): Response
    {
        $conditions = ['user_id' => Auth::id(), 'map_image' => $uuid];

        $activity = null;
        try {
            $activity = Activity::select(['id', 'polyline_summary', 'map_image'])->where($conditions)->firstOrFail();
        } catch (ModelNotFoundException $e) {
            Log::warning('Activity model not found when retrieiving image.', $conditions);

            abort(404, 'Image not found.');
        }

        $image = MapImage::get($uuid, $activity->polyline_summary);

        return $image->response();
    }

}

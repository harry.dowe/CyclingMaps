<?php

namespace App\Http\Controllers;

use Iamstuartwilson\StravaApi;

class BaseStravaController extends Controller
{
    /**
     * @var StravaApi
     */
    protected $strava;

    /**
     * StravaController constructor.
     *
     * @param StravaApi  $strava
     */
    public function __construct(StravaApi $strava)
    {
        $this->strava = $strava;
    }

    /**
     * @return StravaApi
     */
    public function getStrava()
    {
        return $this->strava;
    }

    /**
     * @param StravaApi $strava
     *
     * @return BaseStravaController
     */
    public function setStrava($strava)
    {
        $this->strava = $strava;

        return $this;
    }
}

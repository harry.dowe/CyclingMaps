@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="col-md-12">
            <div id="elevation-map"></div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="application/javascript">
        var jsonString = {!! $elevatonJson !!};
    </script>
    <script src="{{ URL::to('/js/elevation.js') }}"></script>
@endsection

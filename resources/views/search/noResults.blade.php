<template id="no-results-template">
    <div id="no-results-message">
        <p>We didn't find any results for the search <em>"@{{query}}"</em>.</p>
        <a href="." class="clear-all">Clear search</a>
    </div>
</template>

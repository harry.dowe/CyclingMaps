<template id="hit-template">
    <div class="hit-header">
        <a class="hit-name" href="https://strava.com/activities/@{{ strava_id }}">@{{{_highlightResult.name.value}}}</a>
        <span class="hit-date">@{{#helpers.formatDate}}@{{start_date_local}}@{{/helpers.formatDate}}</span>
    </div>
    <div class="hit-stats">
        <div class="hit-duration"><span>@{{#helpers.formatDuration}}@{{elapsed_time}}@{{/helpers.formatDuration}}</span></div>
        <span class="hit-distance">@{{#helpers.formatDistance}}@{{distance}}@{{/helpers.formatDistance}}km</span>
        <div class="hit-elevation-gain"><span>@{{#helpers.formatElevationGain}}@{{total_elevation_gain}}@{{/helpers.formatElevationGain}}m</span></div>
    </div>
    @{{#polyline_summary}}
        <a class="map-image" href="https://strava.com/activities/@{{ strava_id }}">
            <img src="/img/maps/@{{ map_image }}.png">
        </a>
    @{{/polyline_summary}}
</template>

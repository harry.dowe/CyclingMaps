@extends('layouts.app')

@section('header')
    @include('pages.partials.user.header')
@endsection

@section('content')
    @include('pages.partials.user.content')
@endsection

@section('templates')
    @include('search.hit')
    @include('search.noResults')
@endsection

@section('scripts')
    <script type="text/javascript">
        var ALGOLIA_APP_ID = "{{ Config::get('algolia.appId') }}";
        var ALGOLIA_SEARCH_API_KEY = "{{ Config::get('algolia.keys.search') }}";
        var ALGOLIA_INDEX = "{{ $activityIndex }}";
        var ATHLETE_ID = "{{ $user->athlete_id }}";
    </script>

    <script src="{{ URL::to('/js/app.js') }}"></script>
@endsection

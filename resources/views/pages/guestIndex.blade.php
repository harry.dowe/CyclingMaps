@extends('layouts.app')

@section('header')
    @include('pages.partials.guest.header')
@endsection

@section('content')
    @include('pages.partials.guest.content')
@endsection

<header>
    <div class="app-header strava-connect-container">
        <span class="site-title">Cycling Maps</span>
        <a class="strava-connect-button" href="{{ URL::to('authorisation') }}">
            <img src="{{ URL::to('img/strava-connect.png') }}"/>
        </a>
    </div>
</header>

<div class="stripe stripe-default">
    <div>
        <i class="fa fa-clock-o fa-5x" aria-hidden="true"></i>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer blandit consectetur semper. Sed volutpat at urna sed aliquam. Vivamus commodo mi nec est laoreet, ut facilisis orci aliquet. Etiam lorem libero, varius id finibus sit amet, vehicula sit amet ipsum. Cras at est et velit tincidunt porta in non nunc. Vivamus imperdiet venenatis tortor, in vestibulum nisi suscipit et. In hac habitasse platea dictumst. Sed eget ex urna. Curabitur semper sapien sit amet laoreet vestibulum. In ac sem vel tellus consequat porttitor sed eu libero.</p>
    </div>
    <div>
        <i class="fa fa-bicycle fa-5x"></i>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer blandit consectetur semper. Sed volutpat at urna sed aliquam. Vivamus commodo mi nec est laoreet, ut facilisis orci aliquet. Etiam lorem libero, varius id finibus sit amet, vehicula sit amet ipsum. Cras at est et velit tincidunt porta in non nunc. Vivamus imperdiet venenatis tortor, in vestibulum nisi suscipit et. In hac habitasse platea dictumst. Sed eget ex urna. Curabitur semper sapien sit amet laoreet vestibulum. In ac sem vel tellus consequat porttitor sed eu libero.</p>
    </div>
</div>

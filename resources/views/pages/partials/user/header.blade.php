<header>
    <div class="app-header">
        <div class="search-container">
            <div>
                <input class="search-input" id="search-input" type="search" placeholder="Search for activities" title="Search by name" required>
                <button class="search-button search-icon" id="search-input-button"><i class="fa fa-lg fa-search" aria-hidden="true"></i></button>
                <button class="search-button search-clear" id="search-clear-button"><i class="fa fa-lg fa-times-circle" aria-hidden="true"></i></button>
            </div>
            <div class="algolia-powered">
                <span>Powered by</span><a href="https://algolia.com"><img src="{{ URL::to('img/algolia-logo.png') }}"></a>
            </div>
        </div>
        <div class="header-logout">
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" data-toggle="dropdown">
                    <img class="strava-avatar" src="{{$user->profile_medium}}">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="https://www.strava.com/athletes/{{$user->athlete_id}}">Strava Profile</a></li>
                    <li>
                        {{ Form::open(['route' => 'logout']) }}
                        {{ Form::submit('Logout', ['class' => 'logout-button']) }}
                        {{ Form::close() }}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>

$(document).ready(function () {
    const polyline = 'cs{cIxbbX{D{GxV_HrPvOpQd@~OnTn_@xK{CujAmGoUcD`@jGcUgD{PRiTgIya@DcLfJ~@aBq_AeTkl@|AkLiKqa@mM{XdFsZoE}wAmg@gz@wScn@uB^{Eu^unAc~C_Msr@u_@eYeh@aw@XmGmIwU{PmfAS{~@{DaDgPoyAu]}vAsuAw~DGiKdJiYrBkb@oFqg@{N{ZxCcz@gAa{@dQiDlCgr@fUsCbNw[mXg_AnAjCe@yJw@dWaW`U_Wpc@eQ`q@}F~Eed@g@m^q\oThCoGnDwd@rw@wKtGy`@{fAgIq@{GgO_a@fZkP}BmAjJlEti@uXvsAlC`^wH~x@dJ~LbKl\r^ed@lM_m@zT{YdJ}Dz[uxAlKmGjb@uu@b`@{J`]z[fi@M|Sgt@pWyd@~P{O~Bq^rUfq@|Evf@g@uLmMbYkUpDuCdq@_QbDbAf|@_Dby@`Oh\hF|f@mA~[oKd`@PzKluA|}Dt\bsA|Jdl@tEjp@tDbELv|@d[xfBlg@nu@ra@l\`Jdl@hqAdeDxDp[dBUxShn@jg@xy@jE`yA{EnYjMtXxKzb@eBtKxSbl@zAr_AaJ{@GbK~Hpb@`Cpc@yFjVdDFzFnTzDplAa_@kL}OySeQm@yQkQqV`I|CfG';

    google.charts.load('current', {'packages': ['corechart']});

    google.charts.setOnLoadCallback(() => {
        const data = google.visualization.arrayToDataTable(jsonString, false);

        const options = {
            title: 'Elevation',
            curveType: 'function',
            legend: {position: 'none'},
            hAxis: {textPosition: 'none'}
        };

        const chart = new google.visualization.LineChart($('#elevation-map')[0]);
        // google.visualization.events.addListener(chart, 'ready', function () {
        //     $('#elevation-map').html('<img src="' + chart.getImageURI() + '">');
        // });
        chart.draw(data, options);
    });
});


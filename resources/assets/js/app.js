$(document).ready(function () {
    const search = instantsearch({
        appId: ALGOLIA_APP_ID,
        apiKey: ALGOLIA_SEARCH_API_KEY,
        indexName: ALGOLIA_INDEX,
        urlSync: false,
        searchParameters: {
            filters: 'athlete_id = ' + ATHLETE_ID
        }
    });

    search.addWidget(
        instantsearch.widgets.searchBox({
            container: '#search-input',
            scrollTo: '#search-input',
            autofocus: true,
            wrapInput: false
        })
    );

    search.addWidget(
        instantsearch.widgets.hits({
            container: '#hits',
            hitsPerPage: 10,
            templates: {
                item: getTemplate('hit'),
                empty: getTemplate('no-results')
            }
        })
    );

    search.addWidget(
        instantsearch.widgets.pagination({
            container: '#pagination',
            autoHideContainer: true,
            cssClasses: {
                root: 'pagination'
            }
        })
    );

    search.templatesConfig.helpers.formatDistance = function (text, render) {
        return (render(text) / 1000).toFixed(1);
    };

    search.templatesConfig.helpers.formatElevationGain = function (text, render) {
        return Math.round(render(text));
    };

    search.templatesConfig.helpers.formatDuration = function (text, render) {
        return formatTime(render(text), 's');
    };

    search.templatesConfig.helpers.formatDate = function (text, render) {
        return moment.unix(render(text)).format('DD/MM/YYYY');
    };

    const removeLocationWidget = {
        init(options) {
            $('#search-clear-button').click(() => {
                $('#search-input').val('');
                options.helper.setQuery('').search();
            });
        }
    };

    search.addWidget(removeLocationWidget);

    search.start();
});

function getTemplate(templateName) {
    return document.getElementById(templateName + '-template').innerHTML;
}

function formatTime(input, units) {
    const duration = moment().startOf('day').add(input, units);

    if (duration.hour() > 0) {
        return duration.format("H[:]mm[:]ss");
    }

    if (duration.minute() > 0) {
        return duration.format("m[:]ss");
    }

    return duration.format('s');
}

function formatTime2(input, units) {
    const duration = moment().startOf('day').add(input, units);

    if (duration.hour() > 0) {
        return duration.format("H[:]mm");
    }

    if (duration.minute() > 0) {
        return duration.format("m[:]ss");
    }

    return duration.format('s');
}

<?php

return [
    'apiKey' => env('GOOGLE_API_KEY'),
    'elevation' => [
        'url' => 'https://maps.googleapis.com/maps/api/elevation/json'
    ],
    'staticMaps' => [
        'url' => 'https://maps.googleapis.com',
        'url_path' => '/maps/api/staticmap',
        'urlSecret' => env('GOOGLE_STATIC_MAPS_URL_SECRET'),
        'scale' => 2,
        'width' => 545,
        'height' => 200,
        'path' => [
            'weight' => 3,
            'color' => '0x097bedff'
        ],
        'style' => [
            [
                'feature' => 'road.highway',
                'element' => 'geometry',
                'saturation' => -100,
                'lightness' => 35
            ],
            [
                'feature' => 'poi',
                'element' => 'labels',
                'visibility' => 'off'
            ],
            [
                'feature' => 'road',
                'element' => 'labels',
                'visibility' => 'off',
            ],
            [
                'feature' => 'transit',
                'element' => 'labels',
                'visibility' => 'off'
            ],
            [
                'feature' => 'transit',
                'element' => 'labels.icon',
                'visibility' => 'on'
            ],
            [
                'feature' => 'transit.station.airport',
                'element' => 'labels',
                'visibility' => 'on'
            ],
            [
                'feature' => 'transit.station.airport',
                'element' => 'labels.icon',
                'visibility' => 'off'
            ],
            [
                'feature' => 'transit.station.bus',
                'visibility' => 'off'
            ],
            [
                'feature' => 'water',
                'element' => 'labels',
                'visibility' => 'off'
            ]
        ]
    ]
];

<?php

return [
    'appId' => env('ALGOLIA_APP_ID'),
    'keys' => [
        'search' => env('ALGOLIA_SEARCH_API_KEY'),
        'admin' => env('ALGOLIA_ADMIN_API_KEY')
    ]
];

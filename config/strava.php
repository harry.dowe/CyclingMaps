<?php

return [
    'clientId' => env('STRAVA_CLIENT_ID'),
    'clientSecret' => env('STRAVA_CLIENT_SECRET'),
    'accessToken' => env('STRAVA_ACCESS_TOKEN'),
    'accessLevel' => 'public',
    'perPage' => 25
];

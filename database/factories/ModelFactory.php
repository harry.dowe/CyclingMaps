<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Model\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Model\Activity::class, function(Faker\Generator $faker) {
    $movingTime = $faker->numberBetween(0, 43200);

    return [
        'user_id' => \App\Model\User::all()->random()->id,
        'strava_id' => $faker->randomNumber(),
        'name' => $faker->sentence,
        'start_date_local' => $faker->dateTime,
        'distance' => $faker->randomFloat(2, 1000, 250000),
        'moving_time' => $movingTime,
        'elapsed_time' => $faker->numberBetween($movingTime, $movingTime + 3600),
        'total_elevation_gain' => $faker->randomFloat(2, 0, 10000),
        'commute' => $faker->boolean,
        'polyline' => '',
        'polyline_summary' => '',
        'start_lat' => $faker->latitude,
        'start_lng' => $faker->longitude,
        'end_lat' => $faker->latitude,
        'end_lng' => $faker->longitude
    ];
});

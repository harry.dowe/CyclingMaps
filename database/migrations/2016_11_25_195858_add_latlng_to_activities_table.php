<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatlngToActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->float('end_lng')->after('commute');
            $table->float('end_lat')->after('commute');
            $table->float('start_lng')->after('commute');
            $table->float('start_lat')->after('commute');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('start_lat');
            $table->dropColumn('start_lng');
            $table->dropColumn('end_lat');
            $table->dropColumn('end_lng');
        });
    }
}

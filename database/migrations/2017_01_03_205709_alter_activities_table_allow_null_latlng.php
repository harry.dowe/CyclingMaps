<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterActivitiesTableAllowNullLatlng extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->float('start_lat')->nullable()->change();
            $table->float('start_lng')->nullable()->change();
            
            $table->float('end_lat')->nullable()->change();
            $table->float('end_lng')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->float('start_lat')->nullable(false)->change();
            $table->float('start_lng')->nullable(false)->change();

            $table->float('end_lat')->nullable(false)->change();
            $table->float('end_lng')->nullable(false)->change();
        });
    }
}

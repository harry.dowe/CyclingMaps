<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/composer.php';

// Configuration

set('repository', 'git@gitlab.com:harry.dowe/CyclingMaps.git');
set('default_stage', 'demo');

set('shared_files', [
    '.env'
]);

set('create_directories', [
    'public/img',
    'public/img/maps',
    'public/img/avatars'
]);

set('clear_paths', [
    'resources/assets',
    'tests',
    '.git',
    '.gitattributes',
    '.gitignore',
    '.env.example',
    'composer.json',
    'composer.lock',
    'deploy.php',
    'gulpfile.js',
    'LICENSE',
    'package.json',
    'phpunit.xml',
    'readme.md',
    'server.php',
    'Vagrantfile',
    'yarn.lock'
]);

set('clear_files_recursive', [
    '.gitignore',
    '.gitkeep',
    '.gitattributes'
]);

// Servers

server('production', 'ec2-35-156-226-152.eu-central-1.compute.amazonaws.com')
    ->user('dep')
    ->identityFile('~/.ssh/id_rsa.pub')
    ->set('deploy_path', '/var/www/harrydowe.uk')
    ->set('branch', 'master');

server('demo', 'ec2-35-156-226-152.eu-central-1.compute.amazonaws.com')
    ->user('dep')
    ->identityFile('~/.ssh/id_rsa.pub')
    ->set('deploy_path', '/var/www/demo.harrydowe.uk')
    ->set('branch', 'Demo');

// Tasks

desc('Import Strava activities.');
task('strava:import', function () {
    run('php artisan strava:import');
});

desc('Install npm packages.');
task('deploy:npm', function () {
    # Runs into permission issues
    run('npm install --silent');
});

desc('Compile gulp assets.');
task('deploy:gulp', function () {
    run('gulp');
});

desc('Copy images directly into public directory.');
task('deploy:images', function () {
    # Deploying images this way instead of gulp because I struggled to get deploy:npm working
    run('cp -R {{release_path}}/resources/assets/img {{release_path}}/public/img');
});

desc('Recursively find and delete files by name.');
task('deploy:clear_files_recursive', function () {
    $files = get('clear_files_recursive');
    $sudo  = get('clear_use_sudo') ? 'sudo' : '';

    foreach ($files as $file) {
        run('find -L {{release_path}} -type f -name ' . $file . ' -delete');
    }
});

desc('Creates empty directories.');
task('deploy:create_dirs', function () {
    $dirs = get('create_directories');

    foreach ($dirs as $dir) {
        run("if ! [ -d $(echo {{release_path}}/$dir) ]; then mkdir {{release_path}}/$dir; fi");
    }
});

desc('Deploys the app.');
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:images',
    'deploy:writable',
    'deploy:create_dirs',
    'deploy:clear_paths',
    'deploy:clear_files_recursive',
    'artisan:view:clear',
    'artisan:cache:clear',
    'artisan:config:cache',
    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
]);
